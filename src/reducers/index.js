import { combineReducers } from "redux"
const initialState = {
  movieList: Object
}
export const clickReducer = (state = initialState, action) => {
  switch (action.type) {
    case "CLICK_SEARCH":
      return {
        ...state,
        movieList: action.movieList
      }
    default:
      return state
  }
}
export const Reducers = combineReducers({
  clickState: clickReducer
})
