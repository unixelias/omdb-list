import React, { Component } from "react"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { clickButton } from "./actions"
import { Navbar, Table } from "react-materialize"
import axios from "axios"

import "./App.css"

class App extends Component {
  state = {
    searchKey: "",
    movieList: ""
  }

  doSearch = event => {
    this.setState({
      searchKey: event.target.value
    })
  }
  searchMovie = event => {
    this.setState({
      listMovies: () => {
        axios
          .get("http://www.omdbapi.com/?apikey=68b7a486&s=Batman&page=2")
          .then(response => {
            console.log(response)
            return response
          })
          .catch(e => {
            this.errors.push(e)
            alert("Erro no servidor")
          })
      }
    })
  }
  handleClick() {
    axios
      .get("http://www.omdbapi.com/?apikey=68b7a486&s=Batman&page=2")
      .then(response => {
        console.log(response.data.Search)
      })
      .catch(e => {
        this.errors.push(e)
        alert("Erro no servidor")
      })
  }
  render() {
    const { clickButton, movieList } = this.props
    const { searchKey } = this.state
    return (
      <div className="App">
        <Navbar brand="Home" center="true" />
        <div className="container">
          <div>
            <form action="#">
              <div className="input-field row">
                <div className="input-field col s10">
                  <input
                    onChange={this.doSearch}
                    type="text"
                    id="title_search"
                    value={searchKey}
                  />
                  <label for="title_search">Nome do Filme</label>
                </div>
                <div
                  onClick={() => clickButton(searchKey)}
                  className="btn input-field col s2"
                >
                  <span>Buscar</span>
                </div>
              </div>
            </form>
            <h1>{movieList}</h1>
            <p>{searchKey}</p>
          </div>
          <div>
            <div className="button__container">
              <button
                className="button btn input-field col s2"
                onClick={this.handleClick}
              >
                Carrega API
              </button>
              <p>{this.state.username}</p>
            </div>
          </div>
          <Table centered hoverable responsive>
            <thead>
              <tr>
                <th data-field="id">Pôster</th>
                <th data-field="name">Nome do filme</th>
                <th data-field="price">Ano</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <div class="col s2">
                    <img
                      src="https://m.media-amazon.com/images/M/MV5BMzIxMDkxNDM2M15BMl5BanBnXkFtZTcwMDA5ODY1OQ@@._V1_SX300.jpg"
                      alt=""
                      class="responsive-img"
                    />
                  </div>
                </td>
                <td>Batman Begins</td>
                <td>2005</td>
              </tr>
              <tr>
                <td>
                  <div class="col s2">
                    <img
                      src="https://m.media-amazon.com/images/M/MV5BYThjYzcyYzItNTVjNy00NDk0LTgwMWQtYjMwNmNlNWJhMzMyXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg"
                      alt=""
                      class="responsive-img"
                    />
                  </div>
                </td>
                <td>Batman v Superman: Dawn of Justice</td>
                <td>2016</td>
              </tr>
              <tr>
                <td>
                  <div class="col s2">
                    <img
                      src="https://m.media-amazon.com/images/M/MV5BMTYwNjAyODIyMF5BMl5BanBnXkFtZTYwNDMwMDk2._V1_SX300.jpg"
                      alt=""
                      class="responsive-img"
                    />
                  </div>
                </td>
                <td>Batman</td>
                <td>1989</td>
              </tr>
              <tr>
                <td>
                  <div class="col s2">
                    <img
                      src="https://m.media-amazon.com/images/M/MV5BNWY3M2I0YzItNzA1ZS00MzE3LThlYTEtMTg2YjNiOTYzODQ1XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg"
                      alt=""
                      class="responsive-img"
                    />
                  </div>
                </td>
                <td>Batman Forever</td>
                <td>1995</td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
    )
  }
}

const mapStateToProps = store => ({
  searchKey: store.clickState.searchKey,
  movieList: store.clickState.movieList
})
const mapDispatchToProps = dispatch =>
  bindActionCreators({ clickButton }, dispatch)
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
