import axios from "axios"

export function loadSearch(value) {
  return dispatch => {
    return axios
      .get("http://www.omdbapi.com/?apikey=68b7a486&s=Batman")
      .then(response => {
        dispatch(clickButton(response.data.Search))
      })
  }
}

export function clickButton(movies) {
  return {
    type: "CLICK_SEARCH",
    movieList: movies
  }
}
